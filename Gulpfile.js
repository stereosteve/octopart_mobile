var path = require('path')
var gulp = require('gulp')
var less = require('gulp-less')
var concat = require('gulp-concat')
var templateCache = require('gulp-angular-templatecache');


var bootstrapBase = path.join(__dirname, 'vendor', 'bootstrap')
var copyFiles = [
  'app/app.js',
  'app/img/**/*',
  'app/index.html',
]


gulp.task('copy', function() {
  gulp
    .src(copyFiles, {base: 'app'})
    .pipe(gulp.dest('dist'))
})

gulp.task('ngTemplate', function() {
  gulp.src('app/**/*.html')
        .pipe(templateCache({
          module: 'op-mobile'
        }))
        .pipe(gulp.dest('dist'));
})

gulp.task('lessc', function () {
  gulp.src(['./app/less/bootstrap.less', './app/**/*.less'])
    .pipe(concat('bootstrap.less'))
    .pipe(less({
      compress: true,
      paths: [ path.join(bootstrapBase, 'less') ]
    }))
    .pipe(gulp.dest(path.join(bootstrapBase, 'dist', 'css')));
});

gulp.task('less', ['lessc'], function() {
  gulp
    .src(path.join(bootstrapBase, 'dist', '**/*'), {base: path.join(bootstrapBase, 'dist')})
    .pipe(gulp.dest('dist/bootstrap'))
})

gulp.task('default', ['copy', 'ngTemplate', 'less'], function(){
});

gulp.task('watch', ['default'], function() {
  gulp.watch(copyFiles, ['copy']);
  gulp.watch('./app/**/*.html', ['ngTemplate']);
  gulp.watch('./app/**/*.less', ['less']);
  require('./server')
})

var express = require('express')
var superagent = require('superagent')
var url = require('url')
var proxy = require('proxy-middleware')

var app = express()

app.use(express.static('dist'))
app.use('/static/mobile-app', express.static('dist'))

app.use('/api', proxy(url.parse('http://octopart.com/api/')))


app.get('/suggest', function(req, res, next) {
    superagent
        .get('http://octopart.com' + req.url)
        .pipe(res)
})

var port = process.env.PORT || 3000
app.listen(port)
console.log('octopart-mobile on port', port)
angular.module('op-mobile', ['ngRoute', 'ngTouch'])

//
// Routes
//

.config(function($routeProvider) {
    $routeProvider

    .when('/', {
        templateUrl: 'pages/home.html'
    })

    .when('/categories', {
        redirectTo: '/categories/8a1e4714bb3951d9'
    })

    .when('/categories/:category_id', {
        templateUrl: 'pages/category.html',
        resolve: {
            category: function(api, $route) {
                return api.categories.get($route.current.params.category_id, {
                    'include[]': ['imagesets']
                });
            },
        },
        controller: 'categoryCtrl'
    })

    .when('/search', {
        templateUrl: 'pages/search.html',
        resolve: {
            partSearchResponse: function(api, $route) {
                var params = angular.extend($route.current.params, {
                    'include[]': ['short_description', 'imagesets', 'avg_price_v2'],
                    'hide[]': ['offers', 'manufacturer', 'uid_v2'],
                    'slice[imagesets]': '[:1]',
                })
                return api.parts.search(params)
            }
        },
        controller: 'searchCtrl'
    })

    .when('/parts/:part_uid', {
        templateUrl: 'pages/part.html',
        resolve: {
            part: function(api, $route) {
                return api.parts.get($route.current.params.part_uid)
            }
        },
        controller: 'partCtrl'
    })

    .otherwise({
        template: 'not found. <a href="#/">Home</a>'
    })
})


//
// Controllers
//


.controller('searchCtrl', function($scope, partSearchResponse, api, $location, $routeParams) {
    $scope.resp = partSearchResponse
    $scope.parts = partSearchResponse.results

    // pagination variables
    $scope.pageSize = parseInt($routeParams.limit || 10, 10)
    $scope.start = parseInt($routeParams.start || 0, 10)
    $scope.page = $scope.start / $scope.pageSize + 1
    $scope.numPages = Math.ceil(partSearchResponse.hits / $scope.pageSize)

    $scope.lastPage = function() {
        var start = $scope.start - $scope.pageSize
        start = Math.max(start, 0)
        $location.search('start', start)
    }
    $scope.nextPage = function() {
        var start = $scope.start + $scope.pageSize
        $location.search('start', start)
    }
})


.controller('categoryCtrl', function($scope, category, api, $location) {

    $scope.category = category
    var parentIndex = category.ancestor_uids.length - 1
    $scope.parentUid = category.ancestor_uids[parentIndex]
    $scope.parentName = category.ancestor_names[parentIndex]

    // fetch child categories
    api.categories.get_multi({
        'uid[]': category.children_uids,
        'include[]': ['imagesets'],
    }).then(function(children) {
        $scope.children = _.values(children)
    })

    $scope.doCategorySearch = function(category) {
        $location.path('/search')
        $location.search('filter[fields][category_uids][]', category.uid)
    }

})


.controller('partCtrl', function($scope, part, api) {
    $scope.part = part

    $scope.hasSpecs = _.keys(part.specs).length > 0

    // Separate out authorized / non-authorized offers
    var groupedOffers = _.groupBy(part.offers, 'is_authorized')
    $scope.authorizedOffers = groupedOffers[true]
    $scope.nonAuthorizedOffers = groupedOffers[false]

    // pre-process all the part offers
    // to extract single display value for:
    //   currency, price, quanitity
    // Currently just takes the first currency if there are multiple
    part.offers.forEach(function(offer) {
        var currencies = _.keys(offer.prices)
        var currency = currencies[0]
        var priceTuples = offer.prices[currency]
        if (!priceTuples) return;

        offer.display_currency = currency
        offer.display_quantity = priceTuples[0][0]
        offer.display_price = priceTuples[0][1]
    })

    api.categories.get_multi({
        'uid[]': part.category_uids
    }).then(function(categories) {
        $scope.categories = _.values(categories)
    })
})




//
// Directives
//

.directive('topBar', function($window) {
    return {
        templateUrl: 'directives/top-bar.html',
    }
})

.directive('searchBar', function($location, $http, $routeParams) {
    return {
        templateUrl: 'directives/search-bar.html',
        link: link
    }

    function link($scope) {

        $scope.q = $routeParams.q

        $scope.suggest = function(q) {
            if (!q) {
                $scope.suggestions = undefined
                return
            }
            $http({
                method: 'GET',
                url: '/suggest',
                params: {
                    q: q
                }
            }).then(function(r) {
                $scope.suggestions = r.data.split("\n")
            })
        }

        $scope.doSearch = function(q) {
            $location.path('/search')
            $location.search('q', q)
            $location.search('start', 0)
        }

    }
})

.directive('partSearchResults', function() {
    return {
        templateUrl: 'directives/part-search-results.html',
        scope: {
            parts: '=partSearchResults'
        }
    }
})


.directive('partSearchResult', function() {
    return {
        templateUrl: 'directives/part-search-result.html',
        replace: true,
        scope: {
            part: '=partSearchResult'
        },
    }
})

.directive('offerRow', function() {
    return {
        templateUrl: 'directives/offer-row.html',
        scope: {
            offer: '=offerRow'
        }
    }
})

.directive('loadingIndicator', function($rootScope) {
    return {
        templateUrl: 'directives/loading-indicator.html',
        link: function($scope, $el) {
            $rootScope.$on('$routeChangeStart', function() {
                $el.removeClass('hidden')
            })
            $rootScope.$on('$routeChangeSuccess', function() {
                $el.addClass('hidden')
            })
            $rootScope.$on('$routeChangeError', function() {
                $el.addClass('hidden')
            })
        }
    }
})

.directive('footer', function() {
    return {
        templateUrl: 'directives/footer.html',
    }
})


//
// Services
//

.factory('api', function($http) {
    var api = {
        categories: {},
        parts: {},
    }

    // Categories
    api.categories.get = function(uid, q) {
        return get('/api/v3/categories/' + uid, q)
    }

    api.categories.get_multi = function(q) {
        return get('/api/v3/categories/get_multi', q)
    }

    api.categories.search = function(q) {
        return get('/api/v3/categories/search', q)
    }


    // Parts
    api.parts.get = function(uid) {
        return get('/api/v3/parts/' + uid, {
            'include[]': [
                'imagesets',
                'datasheets',
                'external_links',
                'short_description',
                'category_uids',
                'specs'
            ],
            'slice[datasheets]': '[:1]',
            'slice[category_uids]': '[:1]',
        })
    }

    api.parts.search = function(q) {
        return get('/api/v3/parts/search', q)
    }


    // __helpers
    function get(url, params) {
        if (!url) {
            throw new Error("url is required")
        }
        params = params || {}
        params.apikey = MOBILE_ENV.api_key
        url = MOBILE_ENV.api_endpoint + url
        return $http({
            'method': 'GET',
            'url': url,
            'params': params,
            'cache': true,
        }).then(returnData)
    }

    function returnData(response) {
        return response.data
    }

    return api
})